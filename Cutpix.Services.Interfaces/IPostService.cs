﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cutpix.Domain.Models.Blog;

namespace Cutpix.Services.Interfaces
{
    public interface IPostService
    {
        Post GetLastPost(DateTime date);
        IEnumerable<Post> GetLastPosts(DateTime date, int count);
        IList<Post> GetAllPosts();
        void AddNewPost(Post post);
        Post FindPostByLink(string permalink);
    }
}