﻿using System.IO;

namespace Cutpix.Services.Interfaces
{
    public interface IFileService
    {
        object UploadFile(Stream file, string name);
        byte[] GetFileBytes(string id);
        string GetFilename(string id);
    }
}