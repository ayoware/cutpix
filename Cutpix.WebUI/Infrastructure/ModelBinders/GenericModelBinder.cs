﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace Cutpix.WebUI.Infrastructure.ModelBinders
{
    public class GenericModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext,
            Type modelType)
        {
            var type = modelType;
            
            if (modelType.IsGenericType)
            {
                var genericTypeDefinition = modelType.GetGenericTypeDefinition();
                if (genericTypeDefinition == typeof (IDictionary<,>))
                {
                    type = typeof (Dictionary<,>).MakeGenericType(modelType.GetGenericArguments());
                }
                else if ((genericTypeDefinition == typeof (IEnumerable<>)) ||
                          (genericTypeDefinition == typeof (ICollection<>)) ||
                         (genericTypeDefinition == typeof (IList<>)))
                {
                    type = typeof (List<>).MakeGenericType(modelType.GetGenericArguments());
                }

                return Activator.CreateInstance(type);
            }

            if (!modelType.IsAbstract) return Activator.CreateInstance(modelType);

            var concreteTypeName = bindingContext.ModelName + ".TypeName";
            var concreteTypeResult = bindingContext.ValueProvider.GetValue(concreteTypeName);

            if (concreteTypeResult == null)
                throw new Exception("Concrete type for abstract class not specified");

            type = Assembly.GetAssembly(modelType).GetTypes()
                .SingleOrDefault(t => t.IsSubclassOf(modelType) && t.Name == concreteTypeResult.AttemptedValue);

            if (type == null)
                throw new Exception($"Concrete model type {concreteTypeResult.AttemptedValue} not found");

            var instance = Activator.CreateInstance(type);
            bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => instance, type);

            return instance;
        }
    }
}