﻿using System.Web.Mvc;

namespace Cutpix.WebUI.Infrastructure.Filters
{
    public class ModelStateValidationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var modelState = filterContext.Controller.ViewData.ModelState;

            if(!modelState.IsValid)
                filterContext.Result = new RedirectToRouteResult(filterContext.RouteData.Values);

            base.OnActionExecuting(filterContext);
        }
    }
}