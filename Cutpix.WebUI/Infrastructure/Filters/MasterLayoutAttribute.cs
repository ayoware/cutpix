﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cutpix.WebUI.Infrastructure.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class MasterLayoutAttribute : ActionFilterAttribute
    {
        private string _masterPageName;

        public MasterLayoutAttribute(string masterPageName)
        {
            this._masterPageName = masterPageName;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            if (filterContext.Result is ViewResult result)
                result.MasterName = _masterPageName;
        }
    }
}