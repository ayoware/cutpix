﻿// blog create post view javascript

(function (m, $, undefined) {
    "use strict";

    m.init = function(settings) {

        $("#content-list").sortable();

        $(".inputfile").change(function() {

            var file = this.files[0];
            var data = new FormData();
            data.append("FileUpload", file);

            $.ajax({
                type: "POST",
                url: settings.previewUrl,
                contentType: false,
                processData: false,
                data: data,
                success: function(result) {
                    var $cover = $(".post-cover-wrapper");

                    $cover.attr("title", result.FileName);
                    $cover.attr("style", "background-image: url(data:image/png;base64," + result.FileData + ");");
                    $("#display-filename").text(result.FileName);

                    if ($cover.hasClass("collapsed")) {
                        toggleCover($cover, $(".file .toggler"));
                    }
                },
                error: function(xhr, status, p3) {
                    alert(xhr.responseText);
                }
            });
        });

        $(".file .toggler").click(function(e) {

            if (!$("#display-filename").text()) {
                return;
            }

            toggleCover($(".post-cover-wrapper"), this);
        });
    };


    // Private Helpers
    function toggleCover(cover, toggler) {

        if ($(toggler).hasClass("fa-toggle-off")) {
            $(toggler).removeClass("fa-toggle-off").addClass("fa-toggle-on");

            cover.slideDown(300, function() {
                $(this).removeClass("collapsed");
            });
        } else {
            $(toggler).removeClass("fa-toggle-on").addClass("fa-toggle-off");

            cover.slideUp(300, function() {
                $(this).addClass("collapsed");
            });
        }
    }

}(window.main = window.main || {}, jQuery));