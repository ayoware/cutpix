﻿using System.Collections.Generic;
using System.Web.Mvc;


namespace Cutpix.WebUI.HtmlHelpers
{
    public static class ActionLinkHtmlHelper
    {
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper html, string imgSource, string actionName, string controllerName)
        {
            return ActionLinkWithImage(html, imgSource, actionName, controllerName, null);
        }

        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper html, string imgSource, string actionName,
            string controllerName, string cssClass)
        {
            var urlHelper = new UrlHelper(html.ViewContext.RequestContext);

            var imgUrl = urlHelper.Content(imgSource);

            var imgTagBuilder = new TagBuilder("img");
            imgTagBuilder.MergeAttribute("src", imgUrl);
            var imgTagString = imgTagBuilder.ToString(TagRenderMode.SelfClosing);

            var url = urlHelper.Action(actionName, controllerName);

            var linkTagBuilder = new TagBuilder("a") {InnerHtml = imgTagString};
            linkTagBuilder.MergeAttribute("href", url);
            linkTagBuilder.Attributes.Add(new KeyValuePair<string, string>("class", cssClass));

            var htmlLinkString = linkTagBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(htmlLinkString);
        }
    }
}