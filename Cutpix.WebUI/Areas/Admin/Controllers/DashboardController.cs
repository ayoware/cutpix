﻿using System.Collections.Generic;
using System.Web.Mvc;
using Cutpix.Infrastructure.Identity.Owin;
using Cutpix.Web.AutoMapper;
using Cutpix.Web.ViewModels.Dashboard;

namespace Cutpix.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [RouteArea("Admin", AreaPrefix = "admin")]
    public class DashboardController : Controller
    {
        public ApplicationUserManager UserManager { get; }

        public ApplicationRoleManager RoleManager { get; }

        public DashboardController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public DashboardController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
            : this (userManager)
        {
            RoleManager = roleManager;
        }

        #region Action Methods

        [HttpGet]
        public ActionResult ListUsers()
        {
            var models = UserManager.Users
                .ConvertTo<IEnumerable<ApplicationUser>, IEnumerable<ApplicationUserSummeryModel>>();

            return View(models);
        }

        #endregion
    }
}