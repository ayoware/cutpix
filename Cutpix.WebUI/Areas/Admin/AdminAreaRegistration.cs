﻿using System.Web.Mvc;

namespace Cutpix.WebUI.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Admin";


        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Admin",
                url: "admin/{action}/{id}",
                defaults: new { controller = "Dashboard", action = "ListUsers", id = UrlParameter.Optional }
                );
        }
    }
}