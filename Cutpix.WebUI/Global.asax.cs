﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Cutpix.Web.AutoMapper;

namespace Cutpix.WebUI
{
    public class MvcApplication : HttpApplication
    {
        protected readonly string ApplicationName = Assembly.GetExecutingAssembly().GetName().Name;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            BinderConfig.RegisterBinders(ModelBinders.Binders);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.ConfigureProfiles();

            WriteLog($"The application '{ApplicationName}' was successfully started.",
                EventLogEntryType.Information);
        }


        protected void Application_Error(object sender, EventArgs e)
        {
            var unhandledException = Server.GetLastError().GetBaseException();
            var httpException = unhandledException as HttpException;

            WriteLog(httpException?.Message, EventLogEntryType.Error);
        }

        #region Private Helpers

        private void WriteLog(string message, EventLogEntryType type)
        {
            if (!EventLog.SourceExists(ApplicationName))
                EventLog.CreateEventSource(ApplicationName, "Application");

            EventLog.WriteEntry(ApplicationName, message, type);
        }

        #endregion
    }
}