﻿using System.Web;
using System.Web.Mvc;
using Cutpix.Web.ViewModels.Blog;

namespace Cutpix.WebUI.Controllers
{
    [Authorize]
    public class ContentBuilderController : Controller
    {
        [HttpGet]
        public PartialViewResult AddImage()
        {
            return PartialView("_AddContent", new ImageContentFormModel());
        }

        [HttpGet]
        public PartialViewResult AddText()
        {
            return PartialView("_AddContent", new TextContentViewModel());
        }

        [HttpGet, Authorize]
        public ActionResult AddContent(string contentType)
        {
            const string viewName = "_AddContent";

            switch (contentType)
            {
                case "text":
                    return PartialView(viewName, new TextContentViewModel());
                case "image":
                    return PartialView(viewName, new ImageContentFormModel());
                case "audio":
                //return PartialView(viewName, new AudioContent());
                default:
                    throw new HttpException(404, $"Content Type: {contentType} not found.");
            }
        }
    }
}