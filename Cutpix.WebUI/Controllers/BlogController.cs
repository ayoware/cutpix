﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Cutpix.Domain.Models.Blog;
using Cutpix.Services.Interfaces;
using Cutpix.Web.AutoMapper;
using Cutpix.Web.ViewModels.Blog;

namespace Cutpix.WebUI.Controllers
{
    [RoutePrefix("posts")]
    public class BlogController : Controller
    {
        private readonly IPostService _postService;
        private readonly IFileService _fileService;

        public BlogController(IPostService postService, IFileService fileService)
        {
            if (postService == null)
                throw new ArgumentNullException(nameof(postService));
            if (fileService == null)
                throw new ArgumentNullException(nameof(fileService));

            this._postService = postService;
            this._fileService = fileService;
        }

        [HttpGet, Route]
        public ActionResult Posts(int pageSize = 10)
        {
            var result = _postService.GetLastPosts(DateTime.UtcNow, pageSize);
            var models = result.ConvertTo<IEnumerable<Post>, IEnumerable<PostSummaryViewModel>>();

            return View(models);
        }

        [HttpGet, Route("{permalink}")]
        public ActionResult PostDetails(string permalink)
        {
            var post = _postService.FindPostByLink(permalink);
            if (post == null)
                throw new HttpException(404, "404 Not Found");

            var model = post.ConvertTo<Post, PostDetailedViewModel>();

            return View(model);
        }

        [HttpGet, ChildActionOnly]
        public ActionResult LastPost()
        {
            var model = _postService.GetLastPost(DateTime.UtcNow).ConvertTo<Post, PostSummaryViewModel>();
            if (model != null)
                return PartialView("_LastPost", model);

            return new EmptyResult();
        }

        [HttpGet, Route("create"), Authorize]
        public ActionResult CreatePost()
        {
            var model = new NewPostFormViewModel();
            return View(model);
        }

        [HttpPost, Route("create"), Authorize]
        public ActionResult CreatePost(NewPostFormViewModel model)
        {
            var post = model.ConvertTo<NewPostFormViewModel, Post>();

            if (model.File != null)
                post.ImageId = _fileService.UploadFile(model.File.InputStream, model.File.FileName).ToString();

            _postService.AddNewPost(post);

            return RedirectToAction("Posts");
        }
    }
}