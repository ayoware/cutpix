﻿using Cutpix.WebUI.Infrastructure.Filters;
using System.Web.Mvc;

namespace Cutpix.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [MasterLayout("_LayoutKnockout"), Route("about")]
        public ActionResult About()
        {
            return View();
        }

        [HttpGet, Route("data/about")]
        public JsonResult GetAbout()
        {
            var payload = new
            {
                firstName = "Andrei",
                lastName = "Pavlyuk",
                phoneNumber = "+380632218813",
                bio = "I'm a person who thinks deeply according to the facts or exact meaning of something."
            };

            return Json(payload, JsonRequestBehavior.AllowGet);
        }
    }
}