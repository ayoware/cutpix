﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cutpix.Infrastructure.Identity.Owin;
using Cutpix.Web.ViewModels.Account;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SignInStatus = Microsoft.AspNet.Identity.Owin.SignInStatus;

namespace Cutpix.WebUI.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        public ApplicationUserManager UserManager { get; }
        public ApplicationSignInManager SignInManager { get; }
        public IAuthenticationManager AuthenticationManager { get; }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, IAuthenticationManager authManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            AuthenticationManager = authManager;
        }

        #region Action Methods

        [HttpGet, Route("login")]
        public ActionResult Login(string returnUrl)
        {
            var model = new LoginModel {ReturnUrl = returnUrl};

            return View(model);
        }

        [HttpPost, Route("login")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var status = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, true);
            switch (status)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new {returnUrl = model.ReturnUrl});
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [HttpPost]
        public ActionResult Logout()
        {
            // Clear any partial cookies from external or two factor partial sign ins
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                DefaultAuthenticationTypes.ExternalCookie,
                DefaultAuthenticationTypes.TwoFactorCookie,
                DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet, Route("signup")]
        public ActionResult Register()
        {
            return View(new RegisterModel());
        }

        [HttpPost, Route("signup")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser {UserName = model.Email, Email = model.Email};
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new {userId = user.Id, code = code},
                        protocol: Request.Url?.Scheme);

                    await UserManager.SendEmailAsync(user.Id, "Confirm your account",
                        "Please confirm your account by clicking this link: <a href=\"" + callbackUrl +
                        "\">link</a>");

                    ViewBag.Link = callbackUrl;

                    return View("DisplayEmail");
                }

                AddErrors(result);
            }


            return View(model);
        }

        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
                return View("Error");

            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
                return View("ConfirmEmail");

            AddErrors(result);
            return View();
        }

        [HttpGet, Route("~/auth/send-code")]
        public async Task<ActionResult> SendCode(string returnUrl)
        {
            var id = await SignInManager.GetVerifiedUserIdAsync();

            if (id == null)
                throw new Exception("Истекло время ожидания ввода данных аутентификации");

            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(id);
            var factorOptions =
                userFactors.Select(purpose => new SelectListItem {Text = purpose, Value = purpose}).ToList();

            var model = new SendCodeModel
            {
                Providers = factorOptions,
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost, Route("~/auth/send-code")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeModel model)
        {
            if (!ModelState.IsValid)
                return View();

            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
                return View("Error");

            return RedirectToAction("VerifyCode", new {provider = model.SelectedProvider, returnUrl = model.ReturnUrl});
        }

        [HttpGet, Route("~/auth/verify-code")]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl)
        {
            if (!await SignInManager.HasBeenVerifiedAsync())
                return View("Error");

            var user = await UserManager.FindByIdAsync(SignInManager.GetVerifiedUserId());
            if (user != null)
                ViewBag.Status = "For DEMO purposes the current " + provider + " code is: " +
                                 await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);

            return View(new VerifyCodeModel {Provider = provider, ReturnUrl = returnUrl});
        }

        [HttpPost, Route("~/auth/verify-code"), ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var result =
                await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, false, model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        #endregion

        #region External Login Action Methods

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", "Account", new {returnUrl}));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new {returnUrl});
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation",
                        new ExternalLoginConfirmationModel {Email = loginInfo.Email});
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Manage");

            if (ModelState.IsValid)
            {
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                    return View("ExternalLoginFailure");

                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email
                };

                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        #endregion

        #region Private Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                ModelState.AddModelError("", error);
        }

        internal class ExternalLoginResult : HttpUnauthorizedResult
        {
            public string Provider { get; }
            public string RedirectUri { get; }
            public string UserId { get; set; }

            public ExternalLoginResult(string providerName, string redirectUri)
                : this(providerName, redirectUri, null)
            {
            }

            public ExternalLoginResult(string providerName, string redirectUri, string userId)
            {
                this.Provider = providerName;
                this.RedirectUri = redirectUri;
                this.UserId = userId;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties {RedirectUri = RedirectUri};
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, Provider);
            }
        }

        #endregion
    }
}