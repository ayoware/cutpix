﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Cutpix.Infrastructure.Identity.Owin;
using Cutpix.Web.ViewModels.Manage;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace Cutpix.WebUI.Controllers
{
    [RoutePrefix("user"), Authorize]
    public class ManageController : Controller
    {
        private const string XsrfKey = "XsrfId";
        private ApplicationUserManager UserManager { get; }
        private IAuthenticationManager AuthenticationManager { get; }

        public ManageController(ApplicationUserManager userManager, IAuthenticationManager authManager)
        {
            UserManager = userManager;
            AuthenticationManager = authManager;
        }

        [Route("manage")]
        public async Task<ActionResult> Index()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var model = new ManageModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(User.Identity.GetUserId()),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(User.Identity.GetUserId()),
                Logins = await UserManager.GetLoginsAsync(User.Identity.GetUserId()),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(User.Identity.GetUserId())
            };

            return View(model);
        }

        [HttpPost]
        [Route("password/change")]
        public ActionResult ChangePassword()
        {
            return PartialView("_ChangePassword");
        }

        [HttpGet]
        [Route("password/set")]
        public ActionResult SetPassword()
        {
            return View();
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            return user?.PasswordHash != null;
        }
    }
}