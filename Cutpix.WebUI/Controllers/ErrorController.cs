﻿using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Cutpix.WebUI.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult NotFound()
        {
            var controllerName = (string) RouteData.Values["controller"];
            var actionName = (string) RouteData.Values["action"];
            var exception = new HttpException(404, "Houston, We've Got a Problem");

            Response.StatusCode = (int) HttpStatusCode.NotFound;

            var model = new HandleErrorInfo(exception, controllerName, actionName);
            return View("Error", model);
        }
    }
}