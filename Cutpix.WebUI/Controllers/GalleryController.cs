﻿using System;
using System.Web.Mvc;
using Cutpix.Services.Interfaces;

namespace Cutpix.WebUI.Controllers
{
    public class GalleryController : Controller
    {
        protected IFileService FileService { get; set; }

        public GalleryController(IFileService fileService)
        {
            if (fileService == null)
                throw new ArgumentNullException(nameof(fileService));

            this.FileService = fileService;
        }

        public string GetImage(string id)
        {
            if (id == null)
                return string.Empty;

            var bytes = FileService.GetFileBytes(id);
            return $"data:image/png;base64,{Convert.ToBase64String(bytes)}";
        }

        public FileContentResult Image(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var bytes = FileService.GetFileBytes(id);
            return File(bytes, "image/jpeg");
        }

        [HttpPost]
        public JsonResult JsonFilePreview()
        {
            var file = Request.Files["FileUpload"];

            if (file == null) return Json(null, JsonRequestBehavior.DenyGet);

            byte[] buffer;
            using (var fileStream = file.InputStream)
            {
                buffer = new byte[fileStream.Length];
                fileStream.Read(buffer, 0, (int) fileStream.Length);
            }

            return new JsonResult
            {
                Data = new {file.FileName, FileData = Convert.ToBase64String(buffer)},
                JsonRequestBehavior = JsonRequestBehavior.DenyGet,
                MaxJsonLength = int.MaxValue
            };
        }
    }
}