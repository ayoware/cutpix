﻿using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Cutpix.Infrastructure.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using Owin;

[assembly: OwinStartup(typeof (Cutpix.WebUI.Startup))]

namespace Cutpix.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = RegisterDependencies(app);

            // Replace the MVC dependency resolver with Autofac.
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            ConfigureAuth(app);
        }

        private IContainer RegisterDependencies(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.Register(c => app.GetDataProtectionProvider())
                .InstancePerRequest();

            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication)
                .InstancePerRequest();

            builder.RegisterType<ApplicationSignInManager>()
                .AsSelf()
                .InstancePerRequest();

            // Register dependencies in controllers.
            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            // Register dependencies in filter attributes.
            builder.RegisterFilterProvider();

            // Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // Register dependencies modules
            builder.RegisterAssemblyModules(Assembly.Load("Cutpix.Infrastructure.DependencyResolution"));

            return builder.Build();
        }
    }
}