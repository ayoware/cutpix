﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Cutpix.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.LowercaseUrls = true;

            // attribute routing
            routes.MapMvcAttributeRoutes();

            // convension-based routing
            //routes.MapRoute("PostList", "posts", new {controller = "Blog", action = "List"});

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional},
                namespaces: new[] {"Cutpix.WebUI.Controllers"}
                );
        }
    }
}