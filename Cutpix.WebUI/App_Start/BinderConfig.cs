﻿using System.Web.Mvc;
using Cutpix.WebUI.Infrastructure.ModelBinders;

namespace Cutpix.WebUI
{
    public class BinderConfig
    {
        public static void RegisterBinders(ModelBinderDictionary modelBinders)
        {
            modelBinders.DefaultBinder = new GenericModelBinder();
        }
    }
}