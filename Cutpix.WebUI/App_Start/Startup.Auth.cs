﻿using System;
using Cutpix.Infrastructure.Dropbox;
using Cutpix.Infrastructure.Dropbox.Provider;
using Cutpix.Infrastructure.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;

namespace Cutpix.WebUI
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity =
                        SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                            validateInterval: TimeSpan.FromSeconds(5),
                            regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                ClientId = "1092502727077-pv2tt7pi6i6fnhqm07j5fkar08vc7nan.apps.googleusercontent.com",
                ClientSecret = "Egy1LRxtWIpId2A1-el1bYtY",
                Provider = new GoogleOAuth2AuthenticationProvider()
            });

            app.UseDropboxAuthentication(new DropboxOAuth2AuthenticationOptions
            {
                AppKey = "nwa90uy2lq6v7b2",
                AppSecret = "0f1ci3ozfw78fi5",
                Provider = new DropboxAuthenticationProvider()
            });
        }
    }
}