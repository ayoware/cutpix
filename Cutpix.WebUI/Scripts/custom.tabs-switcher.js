﻿(function(window) {

    $(window).on("load", function(e) {

        $(".switch-tabs .tabs span").click(function(e) {

            if ($(this).hasClass("active"))
                return;

            var $switcher = $(this).closest(".switch-tabs");
            $switcher.find(".tabs span.active").removeClass("active");
            $(this).addClass("active");

            // figure out which case to show
            var caseToShow = $(this).attr("rel");

            // show next case
            function showNextCase() {
                $(this).removeClass("active");

                $("#" + caseToShow).slideDown(300, function() {
                    $(this).addClass("active");
                });
            };

            // hide current case
            $switcher.find(".case.active").slideUp(300, showNextCase);
        });

    });

})(window);