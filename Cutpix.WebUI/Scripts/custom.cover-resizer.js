﻿(function (w, $) {

    $(w).on('load', function (e) {
        var viewportHeight = $(w).height();
        $("#cover").height(viewportHeight);
    });

})(window, jQuery);