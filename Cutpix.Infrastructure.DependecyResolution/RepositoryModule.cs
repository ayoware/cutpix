﻿using Autofac;
using Cutpix.Domain.Interfaces;
using Cutpix.Infrastructure.Data.MongoDb;
using Cutpix.Infrastructure.Identity;
using Cutpix.Infrastructure.Identity.Owin;

namespace Cutpix.Infrastructure.DependencyResolution
{
    internal class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<MongoRepository<ApplicationUser>>()
                .As<IRepository<ApplicationUser>>()
                .WithParameter("collectionName", "users")
                .InstancePerRequest();

            builder.RegisterType<MongoRepository<IdentityRole>>()
                .As<IRepository<IdentityRole>>()
                .WithParameter("collectionName", "roles")
                .InstancePerRequest();
        }
    }
}