﻿using System.Configuration;
using System.Linq;
using Autofac;
using Autofac.Core;
using Cutpix.Infrastructure.Data;
using Cutpix.Infrastructure.Data.Contracts;
using Cutpix.Infrastructure.Data.MongoDb.Mapping;

namespace Cutpix.Infrastructure.DependencyResolution
{
    internal class DataModule : Module
    {
        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry,
            IComponentRegistration registration)
        {
            base.AttachToComponentRegistration(componentRegistry, registration);

            // Handle constructor parameters.
            registration.Preparing += this.OnComponentPreparing;
        }

        protected virtual void OnComponentPreparing(object sender, PreparingEventArgs e)
        {
            e.Parameters = e.Parameters.Union(new[]
            {
                new NamedParameter("connectionName", ConfigurationManager.AppSettings["ActiveConnection"]),
            });
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MongoConventionRegistrator>()
                .As<IConventionRegistrator>();

            builder.RegisterType<MongoDataClassMapper>()
                .As<IDataClassMapper>();

            builder.RegisterType<DataContextProvider>()
                .As<IDataContextProvider>()
                .InstancePerRequest();

            builder.Register<IDataContext>(c => c.Resolve<IDataContextProvider>().GetDbContext());

            base.Load(builder);
        }
    }
}