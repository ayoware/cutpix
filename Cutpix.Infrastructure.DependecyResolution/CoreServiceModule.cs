﻿using Autofac;
using Cutpix.Infrastructure.Data;
using Cutpix.Infrastructure.Services;
using Cutpix.Services.Interfaces;

namespace Cutpix.Infrastructure.DependencyResolution
{
    public class CoreServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DynamicContentPostService>().As<IPostService>().InstancePerRequest();
            builder.RegisterType<FileService>().As<IFileService>().InstancePerRequest();

            base.Load(builder);
        }
    }
}