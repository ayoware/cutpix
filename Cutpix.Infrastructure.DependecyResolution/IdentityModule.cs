using Autofac;
using Cutpix.Infrastructure.Identity;
using Cutpix.Infrastructure.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace Cutpix.Infrastructure.DependencyResolution
{
    internal class IdentityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<AdaptiveUserStore<ApplicationUser>>()
                .As<IUserStore<ApplicationUser>>()
                .InstancePerRequest();

            builder.RegisterType<ApplicationUserManager>()
                .AsSelf()
                .InstancePerRequest();

            builder.RegisterType<AdaptiveRoleStore<IdentityRole>>()
                .As<IRoleStore<IdentityRole, string>>()
                .InstancePerRequest();

            builder.RegisterType<ApplicationRoleManager>()
                .AsSelf()
                .InstancePerRequest();
        }
    }
}