﻿using System;
using AutoMapper;
using Cutpix.Domain.Models.Blog;
using Cutpix.Infrastructure.Identity.Owin;
using Cutpix.Web.ViewModels.Blog;
using Cutpix.Web.ViewModels.Dashboard;

namespace Cutpix.Infrastructure.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName => "DomainToViewModel";

        protected override void Configure()
        {
            Mapper.CreateMap<Post, PostSummaryViewModel>()
                .ForMember(vm => vm.Category, opt => opt.MapFrom(m => m.Type));

            Mapper.CreateMap<Post, PostDetailedViewModel>();

            Mapper.CreateMap<ApplicationUser, ApplicationUserSummeryModel>();
        }
    }
}