﻿using AutoMapper;
using Cutpix.Domain.Models.Blog;
using Cutpix.Web.ViewModels.Blog;


namespace Cutpix.Infrastructure.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomain";

        protected override void Configure()
        {
            Mapper.CreateMap<NewPostFormViewModel, Post>();
        }
    }
}