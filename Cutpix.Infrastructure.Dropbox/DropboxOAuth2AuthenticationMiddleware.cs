﻿using System;
using System.Net.Http;
using Cutpix.Infrastructure.Dropbox.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Infrastructure;
using Owin;

namespace Cutpix.Infrastructure.Dropbox
{
    public class DropboxOAuth2AuthenticationMiddleware : AuthenticationMiddleware<DropboxOAuth2AuthenticationOptions>
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public DropboxOAuth2AuthenticationMiddleware(OwinMiddleware next, IAppBuilder app,
            DropboxOAuth2AuthenticationOptions options)
            : base(next, options)
        {
            if (Options.Provider == null)
                Options.Provider = new DropboxAuthenticationProvider();

            if (Options.StateDataFormat == null)
            {
                var dataProtector = app.CreateDataProtector(typeof (DropboxOAuth2AuthenticationMiddleware).FullName,
                    Options.AuthenticationType, "v1");

                Options.StateDataFormat = new PropertiesDataFormat(dataProtector);
            }

            if (string.IsNullOrEmpty(Options.SignInAsAuthenticationType))
                Options.SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType();

            _httpClient = new HttpClient(ResolveHttpMessageHandler())
            {
                Timeout = Options.BackchannelTimeout,
                MaxResponseContentBufferSize = 1024*1024*10
            };

            this._logger = app.CreateLogger<DropboxAuthenticationHandler>();
        }

        protected override AuthenticationHandler<DropboxOAuth2AuthenticationOptions> CreateHandler()
        {
            return new DropboxAuthenticationHandler(_httpClient, _logger);
        }

        private static HttpMessageHandler ResolveHttpMessageHandler()
        {
            var httpMessageHandler = new WebRequestHandler() as HttpMessageHandler;

            return httpMessageHandler;
        }
    }
}