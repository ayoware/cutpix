﻿using System.Security.Claims;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Provider;
using Newtonsoft.Json.Linq;

namespace Cutpix.Infrastructure.Dropbox.Provider
{
    public class DropboxAuthenticatedContext : BaseContext
    {
        public string AccessToken { get; private set; }
        public JObject User { get; private set; }
        public string Id { get; private set; }
        public string Name { get; private set; }
        public ClaimsIdentity Identity { get; set; }
        public AuthenticationProperties Properties { get; set; }

        public DropboxAuthenticatedContext(IOwinContext context, JObject user, string accessToken)
            : base(context)
        {
            AccessToken = accessToken;
            User = user;

            Id = TryGetValue(user, "uid");
            Name = TryGetValue(user, "display_name");
        }

        private static string TryGetValue(JObject user, string propertyName)
        {
            JToken value;
            return user.TryGetValue(propertyName, out value) ? value.ToString() : null;
        }
    }
}