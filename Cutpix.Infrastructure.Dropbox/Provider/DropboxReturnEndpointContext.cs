﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Provider;

namespace Cutpix.Infrastructure.Dropbox.Provider
{
    public class DropboxReturnEndpointContext : ReturnEndpointContext
    {
        public DropboxReturnEndpointContext(IOwinContext context, AuthenticationTicket ticket)
            : base(context, ticket)
        {
        }
    }
}