﻿using System;
using Cutpix.Infrastructure.Dropbox.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Cutpix.Infrastructure.Dropbox
{
    public class DropboxOAuth2AuthenticationOptions : AuthenticationOptions
    {
        public string AppKey { get; set; }
        public string AppSecret { get; set; }
        public ISecureDataFormat<AuthenticationProperties> StateDataFormat { get; set; }
        public PathString CallbackPath { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="IDropboxAuthenticationProvider" /> used in the authentication events
        /// </summary>
        public IDropboxAuthenticationProvider Provider { get; set; }

        /// <summary>
        ///     Gets or sets the name of another authentication middleware which will be responsible for actually issuing a user
        ///     <see cref="T:System.Security.Claims.ClaimsIdentity" />.
        /// </summary>
        public string SignInAsAuthenticationType { get; set; }

        public string Caption
        {
            get { return this.Description.Caption; }
            set { this.Description.Caption = value; }
        }

        public TimeSpan BackchannelTimeout { get; set; }

        public DropboxOAuth2AuthenticationOptions()
            : base("Dropbox")
        {
            Caption = "Dropbox";
            CallbackPath = new PathString("/signin-dropbox");
            AuthenticationMode = AuthenticationMode.Passive;
            BackchannelTimeout = TimeSpan.FromSeconds(60);
        }
    }
}