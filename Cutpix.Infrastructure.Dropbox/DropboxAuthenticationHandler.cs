﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Cutpix.Infrastructure.Dropbox.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Logging;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cutpix.Infrastructure.Dropbox
{
    public class DropboxAuthenticationHandler : AuthenticationHandler<DropboxOAuth2AuthenticationOptions>
    {
        private const string StateCookie = "_DropboxState";
        private const string XmlSchemaString = "http://www.w3.org/2001/XMLSchema#string";
        private const string TokenEndpoint = "https://api.dropbox.com/1/oauth2/token";
        private const string UserInfoEndpoint = "https://api.dropbox.com/1/account/info";
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public DropboxAuthenticationHandler(HttpClient httpClient, ILogger logger)
        {
            this._httpClient = httpClient;
            this._logger = logger;
        }

        protected override Task ApplyResponseChallengeAsync()
        {
            if (!Response.StatusCode.Equals(401))
                return Task.FromResult<object>(null);

            var challenge = Helper.LookupChallenge(Options.AuthenticationType, Options.AuthenticationMode);
            if (challenge != null)
            {
                var baseUri =
                    Request.Scheme +
                    Uri.SchemeDelimiter +
                    Request.Host +
                    Request.PathBase;

                var currentUri =
                    baseUri +
                    Request.Path +
                    Request.QueryString;

                var redirectUri =
                    baseUri +
                    Options.CallbackPath;

                var properties = challenge.Properties;
                if (string.IsNullOrEmpty(properties.RedirectUri))
                    properties.RedirectUri = currentUri;

                // OAuth2 10.12 CSRF
                GenerateCorrelationId(properties);

                var authorizationEndpoint =
                    "https://www.dropbox.com/1/oauth2/authorize" +
                    "?response_type=code" +
                    "&client_id=" + Uri.EscapeDataString(Options.AppKey) +
                    "&redirect_uri=" + Uri.EscapeDataString(redirectUri);

                var cookieOptions = new CookieOptions
                {
                    HttpOnly = true,
                    Secure = Request.IsSecure
                };

                Response.StatusCode = 302;
                Response.Cookies.Append(StateCookie, Options.StateDataFormat.Protect(properties), cookieOptions);
                Response.Headers.Set("Location", authorizationEndpoint);
            }

            return Task.FromResult<object>(null);
        }

        protected override async Task<AuthenticationTicket> AuthenticateCoreAsync()
        {
            AuthenticationProperties properties = null;
            string code = null, state = null;

            var query = Request.Query;
            var values = query.GetValues("code");

            if (values != null && values.Count == 1)
                code = values[0];

            state = Request.Cookies[StateCookie];
            properties = this.Options.StateDataFormat.Unprotect(state);
            if (properties == null) return null;

            // OAuth2 10.12 CSRF
            if (!ValidateCorrelationId(properties, _logger))
                return new AuthenticationTicket(null, properties);

            // Check for error
            if (Request.Query.Get("error") != null)
                return new AuthenticationTicket(null, properties);

            var requestPrefix = Request.Scheme + "://" + Request.Host;
            var redirectUri = requestPrefix + Request.PathBase + Options.CallbackPath;

            // Build up the body for the token request
            var body = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("code", code),
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
                new KeyValuePair<string, string>("redirect_uri", redirectUri),
                new KeyValuePair<string, string>("client_id", Options.AppKey),
                new KeyValuePair<string, string>("client_secret", Options.AppSecret)
            };

            // Request the token
            var tokenResponse = await _httpClient.PostAsync(TokenEndpoint, new FormUrlEncodedContent(body));
            tokenResponse.EnsureSuccessStatusCode();
            var oauthTokenResponse = await tokenResponse.Content.ReadAsStringAsync();

            // Deserializes the token response
            dynamic response = JsonConvert.DeserializeObject<dynamic>(oauthTokenResponse);
            var accessToken = (string) response.access_token;

            // Get the Dropbox user
            var userRequestUri = UserInfoEndpoint + "?access_token=" + Uri.EscapeDataString(accessToken);
            var graphResponse = await _httpClient.GetAsync(userRequestUri, Request.CallCancelled);
            graphResponse.EnsureSuccessStatusCode();
            var userGraphResponse = await graphResponse.Content.ReadAsStringAsync();
            var user = JObject.Parse(userGraphResponse);

            var context = new DropboxAuthenticatedContext(Context, user, accessToken)
            {
                Identity = new ClaimsIdentity(Options.AuthenticationType,
                    ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType)
            };

            // Adding Claims to DropboxAuthenticatedContext Identity
            if (!string.IsNullOrEmpty(context.Id))
                context.Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, context.Id,
                    XmlSchemaString, this.Options.AuthenticationType));

            if (!string.IsNullOrEmpty(context.Name))
                context.Identity.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, context.Name,
                    XmlSchemaString, this.Options.AuthenticationType));

            context.Properties = properties;

            await this.Options.Provider.Authenticated(context);
            return new AuthenticationTicket(context.Identity, properties);
        }

        public override Task<bool> InvokeAsync()
        {
            return InvokeReplyPathAsync();
        }

        private async Task<bool> InvokeReplyPathAsync()
        {
            if (Options.CallbackPath.HasValue && Options.CallbackPath == Request.Path)
            {
                var ticket = await AuthenticateAsync();
                if (ticket == null)
                {
                    Response.StatusCode = 500;
                    return true;
                }

                var context = new DropboxReturnEndpointContext(Context, ticket);
                context.SignInAsAuthenticationType = Options.SignInAsAuthenticationType;
                context.RedirectUri = ticket.Properties.RedirectUri;

                await Options.Provider.ReturnEndpoint(context);

                if (context.SignInAsAuthenticationType != null && context.Identity != null)
                {
                    var grantIdentity = context.Identity;
                    if (!string.Equals(grantIdentity.AuthenticationType, context.SignInAsAuthenticationType,
                        StringComparison.Ordinal))
                    {
                        grantIdentity = new ClaimsIdentity(grantIdentity.Claims, context.SignInAsAuthenticationType,
                            grantIdentity.NameClaimType, grantIdentity.RoleClaimType);
                    }

                    Context.Authentication.SignIn(context.Properties, grantIdentity);
                }

                if (!context.IsRequestCompleted && context.RedirectUri != null)
                {
                    var redirectUri = context.RedirectUri;
                    if (context.Identity == null)
                    {
                        // add a redirect hint that sign-in failed in some way
                        redirectUri = WebUtilities.AddQueryString(redirectUri, "error", "access_denied");
                    }
                    Response.Redirect(redirectUri);
                    context.RequestCompleted();
                }

                return context.IsRequestCompleted;
            }

            return false;
        }
    }
}