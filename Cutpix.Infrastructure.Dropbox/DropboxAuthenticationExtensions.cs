﻿using System;
using Owin;

namespace Cutpix.Infrastructure.Dropbox
{
    public static class DropboxAuthenticationExtensions
    {
        public static IAppBuilder UseDropboxAuthentication(this IAppBuilder app,
            DropboxOAuth2AuthenticationOptions options)
        {
            if (app == null)
                throw new ArgumentNullException(nameof(app));
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            return app.Use(typeof (DropboxOAuth2AuthenticationMiddleware), app, options);
        }
    }
}