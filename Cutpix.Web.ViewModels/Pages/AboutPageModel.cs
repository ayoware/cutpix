﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cutpix.Web.ViewModels.Pages
{
    public class AboutPageModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Bio { get; set; }

        public string FullName
        {
            get { return this.FirstName + " " + this.LastName; }
        }
    }
}
