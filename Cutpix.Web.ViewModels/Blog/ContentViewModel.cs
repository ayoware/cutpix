﻿namespace Cutpix.Web.ViewModels.Blog
{
    public abstract class ContentViewModel
    {
        public abstract string Name { get; set; }
    }
}