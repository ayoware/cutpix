﻿using System;
using System.Collections.Generic;

namespace Cutpix.Web.ViewModels.Blog
{
    public class PostSummaryViewModel
    {
        public string Id { get; set; }
        public string ImageId { get; set; }
        public string Title { get; set; }
        public string Permalink { get; set; }
        public DateTime Created { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public List<string> Tags { get; set; }
        public string Category { get; set; }
    }
}