﻿using System;
using System.Collections.Generic;

namespace Cutpix.Web.ViewModels.Blog
{
    public class PostDetailedViewModel
    {
        public string Id { get; set; }
        public string ImageId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public List<ContentViewModel> ContentList { get; set; }
        public List<string> Tags { get; set; }
    }
}