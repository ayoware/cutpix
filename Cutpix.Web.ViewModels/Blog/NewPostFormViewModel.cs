﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Cutpix.Web.ViewModels.Blog
{
    public class NewPostFormViewModel
    {
        [DisplayName("Выбрать обложку")]
        public HttpPostedFileBase File { get; set; }

        [DisplayName("Название"), Required]
        public string Title { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        [DisplayName("Ссылка"), Required]
        public string Permalink { get; set; }

        public string Author { get; set; }
        public List<string> Tags { get; set; }

        public ICollection<ContentViewModel> ContentList { get; set; }

        public NewPostFormViewModel()
        {
            this.ContentList = new List<ContentViewModel>();
            this.Tags = new List<string>();
        }
    }
}