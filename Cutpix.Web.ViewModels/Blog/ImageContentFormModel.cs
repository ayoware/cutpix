﻿using System.Web;

namespace Cutpix.Web.ViewModels.Blog
{
    public class ImageContentFormModel : ContentViewModel
    {
        public virtual string Id { get; set; }
        public override string Name { get; set; }
        public HttpPostedFileBase File { get; set; }

        //public byte[] GetFileBytes()
        //{
        //    byte[] buffer;
        //    using (var fileStream = File.InputStream)
        //    {
        //        buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int) fileStream.Length);
        //    }

        //    return buffer;
        //}

        public override string ToString()
        {
            return Name;
        }
    }
}