﻿namespace Cutpix.Web.ViewModels.Blog
{
    public class ImageContentInfoModel : ContentViewModel
    {
        public string Id { get; set; }
        public override string Name { get; set; }
    }
}