﻿using System.ComponentModel;
using System.Text;

namespace Cutpix.Web.ViewModels.Blog
{
    public class TextContentViewModel : ContentViewModel
    {
        [DisplayName("Заголовок")]
        public override string Name { get; set; }

        [DisplayName("Текст")]
        public virtual string Body { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.AppendLine(Name);
            builder.AppendLine(Body);

            return builder.ToString();
        }
    }
}