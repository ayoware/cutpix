﻿using System.Collections.Generic;

namespace Cutpix.Web.ViewModels.Account
{
    public class SendCodeModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
    }
}