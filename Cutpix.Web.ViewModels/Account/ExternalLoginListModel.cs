﻿namespace Cutpix.Web.ViewModels.Account
{
    public class ExternalLoginListModel
    {
        public string ReturnUrl { get; set; }
    }
}