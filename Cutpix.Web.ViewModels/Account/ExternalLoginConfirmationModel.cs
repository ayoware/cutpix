﻿using System.ComponentModel.DataAnnotations;

namespace Cutpix.Web.ViewModels.Account
{
    public class ExternalLoginConfirmationModel
    {
        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
    }
}