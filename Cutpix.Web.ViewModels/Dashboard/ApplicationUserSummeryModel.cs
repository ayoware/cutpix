﻿namespace Cutpix.Web.ViewModels.Dashboard
{
    public class ApplicationUserSummeryModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}