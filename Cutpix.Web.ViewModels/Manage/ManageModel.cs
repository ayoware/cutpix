﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace Cutpix.Web.ViewModels.Manage
{
    public class ManageModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }
}