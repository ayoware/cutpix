﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cutpix.Domain.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace Cutpix.Infrastructure.Data.FileStorage
{
    public class MongoFileStorage : IFileStorage
    {
        private readonly IGridFSBucket _bucket;

        public MongoFileStorage(IGridFSBucket bucket)
        {
            this._bucket = bucket;
        }

        public object AddFile(Stream stream, string fileName)
        {
            return _bucket.UploadFromStreamAsync(fileName, stream).Result;
        }

        public Stream Open(object key)
        {
            var openStreamTask = Task.Run(() => OpenAsync(key));
            openStreamTask.Wait();

            return openStreamTask.Result;
        }

        public string GetFileName(object id)
        {
            var filter = Builders<GridFSFileInfo>.Filter.Eq("_id", ParseObjectIdInternal(id));
            var runningTask = Task.Run(() => GetFileNameAsync(filter));

            runningTask.Wait();
            return runningTask.Result;
        }


        protected virtual async Task<string> GetFileNameAsync(FilterDefinition<GridFSFileInfo> filter)
        {
            using (var cursor = await _bucket.FindAsync(filter))
            {
                var fileInfo = (await cursor.ToListAsync()).FirstOrDefault();
                if (fileInfo != null)
                    return fileInfo.Filename;

                return await Task.FromResult(string.Empty);
            }
        }

        protected virtual async Task<Stream> OpenAsync(object id)
        {
            using (var downloadStream = await _bucket.OpenDownloadStreamAsync(ParseObjectIdInternal(id)))
            {
                var buffer = new byte[downloadStream.Length];
                await downloadStream.ReadAsync(buffer, 0, (int) downloadStream.Length);

                return new MemoryStream(buffer);
            }
        }

        #region Private Internal Helpers

        private static ObjectId ParseObjectIdInternal(object key)
        {
            ObjectId id;
            ObjectId.TryParse(key.ToString(), out id);
            return id;
        }

        #endregion
    }
}