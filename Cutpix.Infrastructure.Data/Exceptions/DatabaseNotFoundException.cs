﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cutpix.Infrastructure.Data.Exceptions
{
    /// <summary>
    /// Fires then the wrong database name was used or data base was not found.
    /// </summary>
    [Serializable]
    public class DatabaseNotFoundException : Exception
    {
        public DatabaseNotFoundException(Type exceptiontype, string errorMessage)
            : base($"{exceptiontype.Name} exception. Message: {errorMessage}")
        {
        }

        public DatabaseNotFoundException(string dataBaseName)
            : this(typeof (DatabaseNotFoundException), $"Database `{dataBaseName}` was not found.")
        {
        }
    }
}