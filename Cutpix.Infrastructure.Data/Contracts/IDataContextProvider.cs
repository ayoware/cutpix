﻿namespace Cutpix.Infrastructure.Data.Contracts
{
    public interface IDataContextProvider
    {
        IDataContext GetDbContext();
    }
}