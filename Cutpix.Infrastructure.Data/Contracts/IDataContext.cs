﻿using System;
using Cutpix.Domain.Interfaces;
using MongoDB.Driver;

namespace Cutpix.Infrastructure.Data.Contracts
{
    public interface IDataContext : IDisposable
    {
        string DatabaseName { get; }
        IMongoDatabase Database { get; }
        IFileStorage FileStorage { get; }
    }
}