﻿using System;

namespace Cutpix.Infrastructure.Data.Contracts
{
    public interface IConventionRegistrator
    {
        void RegisterGlobalRules();
        void Register(Type entityType);
    }
}