﻿namespace Cutpix.Infrastructure.Data.Contracts
{
    public interface IDataClassMapper
    {
        void InitializeMappings();
    }
}