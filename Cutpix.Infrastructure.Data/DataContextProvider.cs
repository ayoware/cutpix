﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using Cutpix.Infrastructure.Data.Exceptions;

namespace Cutpix.Infrastructure.Data
{
    using Contracts;
    using MongoDb;

    public class DataContextProvider : IDataContextProvider
    {
        public enum ProviderType
        {
            MongoDbClient = 0,
            SqlClient = 1,
            XmlDataClient = 10,
            StreamDataClient = 11,
        }

        private readonly ConnectionStringSettings _settings;

        public DataContextProvider(string connectionName, IDataClassMapper classMapper)
        {
            classMapper.InitializeMappings();
            _settings = ConfigurationManager.ConnectionStrings[connectionName];
        }

        public IDataContext GetDbContext()
        {
            var provider = ParseProviderType(_settings.ProviderName);

            switch (provider)
            {
                case ProviderType.MongoDbClient:
                    return new MongoDbContext(_settings.ConnectionString);
                case ProviderType.SqlClient:
                case ProviderType.XmlDataClient:
                case ProviderType.StreamDataClient:
                default:
                    throw new DatabaseNotFoundException(_settings.Name);
            }
        }

        #region Private Helpers

        private static ProviderType ParseProviderType(string providerName)
        {
            return (ProviderType) Enum.Parse(typeof (ProviderType), providerName.Split('.').Last());
        }

        #endregion
    }
}