﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Cutpix.Infrastructure.Data.MongoDb
{
    using Domain.Interfaces;
    using Contracts;

    public class MongoRepository<T> : IRepository<T> where T : class
    {
        public MongoRepository(IDataContext context, string collectionName)
        {
            Collection = context.Database.GetCollection<T>(collectionName);
        }

        #region Properties

        public IMongoCollection<T> Collection { get; }

        #endregion

        #region IRepository<T> Implementation

        public void Add(T entity)
        {
            var result = Task.FromResult(Collection.InsertOneAsync(entity)).Result;
            var status = result.Status;
        }

        public bool Update(T entity, string field, object value)
        {
            var doc = entity.ToBsonDocument();
            var filter = Builders<T>.Filter.Eq(doc.First(x => x.Name == "_id").Name, doc["_id"]);
            var update = Builders<T>.Update.Set(field, BsonValue.Create(value));

            var updateResult = Collection.UpdateOneAsync(filter, update).Result;

            return updateResult.ModifiedCount > 0;
        }

        public bool Replace(T entity)
        {
            var doc = entity.ToBsonDocument();
            var filter = Builders<T>.Filter.Eq(doc.First(x => x.Name == "_id").Name, doc["_id"]);

            var replaceOneResult = Collection.ReplaceOneAsync(filter, entity).Result;

            return replaceOneResult.ModifiedCount > 0;
        }

        public bool Delete(object key)
        {
            var filter = Builders<T>.Filter.Eq("_id", GetEntityId(key));
            var deleteResult = Collection.DeleteOneAsync(filter).Result;

            return deleteResult.DeletedCount > 0;
        }

        public T Single(object key)
        {
            var filter = Builders<T>.Filter.Eq("_id", GetEntityId(key));

            return Collection.Find(filter).SingleAsync().Result;
        }

        public T Single(Expression<Func<T, bool>> expression)
        {
            return Collection.Find(expression).FirstOrDefaultAsync().Result;
        }

        public IEnumerable<T> All(Expression<Func<T, bool>> expression = null)
        {
            FilterDefinition<T> filter = new BsonDocument();

            if (expression != null)
                filter = Builders<T>.Filter.Where(expression);

            return Collection.Find(filter).ToListAsync().Result;
        }

        #endregion

        #region Private Helpers

        private BsonValue GetEntityId(object key)
        {
            var value = BsonValue.Create(key);

            ObjectId id;
            if (ObjectId.TryParse(key.ToString(), out id))
                value = id;

            return value;
        }

        #endregion
    }
}