﻿using Cutpix.Domain.Models.Blog;
using MongoDB.Bson.Serialization;
namespace Cutpix.Infrastructure.Data.MongoDb.Mapping
{
    public class TextContentMap : MongoEntityBaseClassMap<TextContent>
    {
        public override void Map(BsonClassMap<TextContent> classMap)
        {
            classMap.AutoMap();
        }
    }
}