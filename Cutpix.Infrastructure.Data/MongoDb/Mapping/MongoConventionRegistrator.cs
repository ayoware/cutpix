﻿using System;
using Cutpix.Infrastructure.Data.Contracts;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace Cutpix.Infrastructure.Data.MongoDb.Mapping
{
    public class MongoConventionRegistrator : IConventionRegistrator
    {
        public void RegisterGlobalRules()
        {
            Register(type => true);
        }

        public void Register(Type entityType)
        {
            if (BsonClassMap.IsClassMapRegistered(entityType)) return;

            Register(type => type == entityType);
        }

        private void Register(Func<Type, bool> filter)
        {
            ConventionRegistry.Register(
                "Data Map Convention",
                CreateMapConventionPack(),
                filter);
        }

        protected virtual IConventionPack CreateMapConventionPack()
        {
            var pack = new ConventionPack
            {
                new IgnoreIfNullConvention(true),
                new CamelCaseElementNameConvention(),
            };

            return pack;
        }
    }
}