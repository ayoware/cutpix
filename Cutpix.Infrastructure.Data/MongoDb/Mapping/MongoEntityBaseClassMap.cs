﻿using System;
using Cutpix.Infrastructure.Data.Contracts;
using MongoDB.Bson.Serialization;

namespace Cutpix.Infrastructure.Data.MongoDb.Mapping
{
    public abstract class MongoEntityBaseClassMap<T> : BsonClassMap<T>
    {
        protected MongoEntityBaseClassMap()
        {
            if (IsClassMapRegistered(typeof (T))) return;

            RegisterClassMap<T>(Map);
        }

        public abstract void Map(BsonClassMap<T> classMap);
    }
}