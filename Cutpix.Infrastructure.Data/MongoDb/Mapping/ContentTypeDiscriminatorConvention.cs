﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Conventions;

namespace Cutpix.Infrastructure.Data.MongoDb.Mapping
{
    public class ContentTypeDiscriminatorConvention : IDiscriminatorConvention
    {
        public string ElementName => "_contentType";

        public Type GetActualType(IBsonReader bsonReader, Type nominalType)
        {
            throw new NotImplementedException();
        }

        public BsonValue GetDiscriminator(Type nominalType, Type actualType)
        {
            throw new NotImplementedException();
        }
    }
}