﻿using System;
using System.Linq;
using System.Reflection;
using Cutpix.Infrastructure.Data.Contracts;

namespace Cutpix.Infrastructure.Data.MongoDb.Mapping
{
    public class MongoDataClassMapper : IDataClassMapper
    {
        private readonly IConventionRegistrator _conventionRegistrator;

        public MongoDataClassMapper(IConventionRegistrator conventionRegistrator)
        {
            _conventionRegistrator = conventionRegistrator;
        }

        public void InitializeMappings()
        {
            _conventionRegistrator.RegisterGlobalRules();

            var classMapTypes = Assembly.GetAssembly(typeof (MongoEntityBaseClassMap<>)).GetTypes()
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                               type.BaseType.GetGenericTypeDefinition() == typeof (MongoEntityBaseClassMap<>));

            foreach (var type in classMapTypes)
                Activator.CreateInstance(type);
        }
    }
}