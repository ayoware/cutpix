﻿using Cutpix.Domain.Interfaces;
using Cutpix.Infrastructure.Data.Contracts;
using Cutpix.Infrastructure.Data.FileStorage;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace Cutpix.Infrastructure.Data.MongoDb
{
    public class MongoDbContext : IDataContext
    {
        public string DatabaseName { get; }
        public IMongoDatabase Database { get; }
        public IFileStorage FileStorage { get; }

        public MongoDbContext(string connectionString)
        {
            var url = new MongoUrl(connectionString);
            var client = new MongoClient(url);

            DatabaseName = url.DatabaseName;
            Database = client.GetDatabase(DatabaseName);
            FileStorage = new MongoFileStorage(new GridFSBucket(Database));
        }

        public void Dispose()
        {
        }
    }
}