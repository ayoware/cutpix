﻿using System.Linq;
using Cutpix.Domain.Interfaces;
using MongoDB.Driver;

namespace Cutpix.Infrastructure.Identity
{
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;

    public class RoleStore<TRole> : IRoleStore<TRole>
        where TRole : IdentityRole
    {
        private readonly IMongoCollection<TRole> _roles;

        public RoleStore(IMongoCollection<TRole> roles)
        {
            _roles = roles;
        }

        public virtual void Dispose()
        {
            // no need to dispose of anything, mongodb handles connection pooling automatically
        }

        public virtual Task CreateAsync(TRole role)
        {
            return _roles.InsertOneAsync(role);
        }

        public virtual Task UpdateAsync(TRole role)
        {
            return _roles.ReplaceOneAsync(r => r.Id == role.Id, role);
        }

        public virtual Task DeleteAsync(TRole role)
        {
            return _roles.DeleteOneAsync(r => r.Id == role.Id);
        }

        public virtual Task<TRole> FindByIdAsync(string roleId)
        {
            return _roles.Find(r => r.Id == roleId).FirstOrDefaultAsync();
        }

        public virtual Task<TRole> FindByNameAsync(string roleName)
        {
            return _roles.Find(r => r.Name == roleName).FirstOrDefaultAsync();
        }
    }
}