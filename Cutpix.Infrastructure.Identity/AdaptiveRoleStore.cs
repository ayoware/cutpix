﻿using System.Threading.Tasks;
using Cutpix.Domain.Interfaces;
using Microsoft.AspNet.Identity;

namespace Cutpix.Infrastructure.Identity
{
    public class AdaptiveRoleStore<TRole> : IRoleStore<TRole>
        where TRole : IdentityRole
    {
        private IRepository<TRole> _repository;

        public AdaptiveRoleStore(IRepository<TRole> repository)
        {
            _repository = repository;
        }

        public Task CreateAsync(TRole role)
        {
            return Task.Run(() => _repository.Add(role));
        }

        public Task DeleteAsync(TRole role)
        {
            return Task.Run(() => _repository.Delete(role.Id));
        }

        public Task<TRole> FindByIdAsync(string roleId)
        {
            return Task<TRole>.Factory.StartNew(() => _repository.Single(roleId));
        }

        public Task<TRole> FindByNameAsync(string roleName)
        {
            return Task<TRole>.Factory.StartNew(() => _repository.Single(r => r.Name == roleName));
        }

        public Task UpdateAsync(TRole role)
        {
            return Task.Run(() => _repository.Replace(role));
        }

        public void Dispose()
        {
            _repository = null;
        }
    }
}