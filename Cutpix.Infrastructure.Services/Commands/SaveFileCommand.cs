﻿using System;
using System.Diagnostics;
using System.IO;
using Cutpix.Domain.Interfaces;
using Cutpix.Domain.Models.Blog;

namespace Cutpix.Infrastructure.Services.Commands
{
    public class SaveFileCommand : ICommand<Content>
    {
        private readonly IFileStorage _fileStorage;
        private BinaryContent _content;

        private SaveFileCommand(IFileStorage fileStorage)
        {
            _fileStorage = fileStorage;
        }

        public static ICommand<Content> CreateInstance(IFileStorage fileStorage)
        {
            return new SaveFileCommand(fileStorage);
        }

        public string Execute()
        {
            try
            {
                using (var file = new MemoryStream(_content.GetBinaryData()))
                    return _fileStorage.AddFile(file, _content.Name).ToString();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

            return null;
        }

        public ICommand<Content> Set(Content data)
        {
            _content = data as BinaryContent;

            return this;
        }
    }
}