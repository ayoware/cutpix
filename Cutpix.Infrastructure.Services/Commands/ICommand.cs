using Cutpix.Domain.Models.Blog;

namespace Cutpix.Infrastructure.Services.Commands
{
    public interface ICommand<in T>
    {
        string Execute();
        ICommand<T> Set(T data);
    }
}