﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cutpix.Domain.Interfaces;
using Cutpix.Domain.Models.Blog;
using Cutpix.Infrastructure.Data.Contracts;
using Cutpix.Infrastructure.Data.MongoDb;
using Cutpix.Services.Interfaces;

namespace Cutpix.Infrastructure.Services
{
    public class PostService : IPostService
    {
        private IRepository<Post> _postRepo;

        protected IDataContext DataContext { get; }

        protected virtual IRepository<Post> PostRepository
            => _postRepo ?? (_postRepo = new MongoRepository<Post>(DataContext, "posts"));

        public PostService(IDataContext dataContext)
        {
            DataContext = dataContext;
        }

        public Post GetLastPost(DateTime date)
        {
            var post = PostRepository.All(d => d.Created <= date).OrderBy(d => d.Created).LastOrDefault();
            return post;
        }

        public IEnumerable<Post> GetLastPosts(DateTime date, int count)
        {
            return PostRepository.All(p => p.Created <= date).OrderBy(d => d.Created).Take(count);
        }

        public IList<Post> GetAllPosts()
        {
            return PostRepository.All().ToList();
        } 

        public virtual void AddNewPost(Post post)
        {
            post.Type = "blog-post";
            post.Created = DateTime.UtcNow;

            PostRepository.Add(post);
        }

        public Post FindPostByLink(string permalink)
        {
            return PostRepository.Single(p => p.Permalink.ToLower() == permalink.ToLower());
        }
    }
}