﻿using System.IO;
using Cutpix.Infrastructure.Data.Contracts;
using Cutpix.Services.Interfaces;

namespace Cutpix.Infrastructure.Services
{
    public class FileService : IFileService
    {
        private readonly IDataContext _context;

        public FileService(IDataContextProvider provider)
        {
            _context = provider.GetDbContext();
        }

        public object UploadFile(Stream file, string name)
        {
            using (file)
                return _context.FileStorage.AddFile(file, name);
        }

        public byte[] GetFileBytes(string id)
        {
            var stream = _context.FileStorage.Open(id);
            using (var reader = new BinaryReader(stream))
            {
                return reader.ReadBytes((int) stream.Length);
            }
        }

        public string GetFilename(string id)
        {
            return _context.FileStorage.GetFileName(id);
        }
    }
}