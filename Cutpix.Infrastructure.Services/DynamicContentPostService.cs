﻿using System.Linq;
using Cutpix.Domain.Models.Blog;
using Cutpix.Infrastructure.Data.Contracts;
using Cutpix.Infrastructure.Services.Commands;

namespace Cutpix.Infrastructure.Services
{
    public sealed class DynamicContentPostService : PostService
    {
        private readonly ICommand<Content> _saveCmd; 

        public DynamicContentPostService(IDataContext dataContext)
            : base(dataContext)
        {
            _saveCmd = SaveFileCommand.CreateInstance(DataContext.FileStorage);
        }

        public override void AddNewPost(Post post)
        {
            foreach (var content in post.ContentList.OfType<BinaryContent>())
                content.Id = _saveCmd.Set(content).Execute();

            base.AddNewPost(post);
        }
    }
}