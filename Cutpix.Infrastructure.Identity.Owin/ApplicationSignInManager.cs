﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Cutpix.Infrastructure.Identity.Owin
{
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager,
            IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options,
            IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        //public override async Task SignInAsync(ApplicationUser user, bool isPersistent, bool rememberBrowser)
        //{
        //    // Clear any partial cookies from external or two factor partial sign ins
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie,
        //        DefaultAuthenticationTypes.TwoFactorCookie);

        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

        //    if (rememberBrowser)
        //    {
        //        var rememberBrowserIdentity = AuthenticationManager.CreateTwoFactorRememberBrowserIdentity(user.Id);

        //        AuthenticationManager.SignIn(
        //            new AuthenticationProperties {IsPersistent = isPersistent}, identity, rememberBrowserIdentity);
        //    }
        //    else
        //    {
        //        AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = isPersistent}, identity);
        //    }
        //}
    }
}