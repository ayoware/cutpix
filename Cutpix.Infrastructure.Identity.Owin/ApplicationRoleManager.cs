using Cutpix.Infrastructure.Data.Contracts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Cutpix.Infrastructure.Identity.Owin
{
    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var db = context.Get<IDataContext>().Database;
            var collection = db.GetCollection<IdentityRole>("Roles");
            var manager = new ApplicationRoleManager(new RoleStore<IdentityRole>(collection));

            return manager;
        }
    }
}