﻿using System.ServiceProcess;

namespace Cutpix.WindowsService.Toaster
{
    internal static class Program
    {
        private static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new ToasterHostingService()
            };

            ServiceBase.Run(servicesToRun);
        }
    }
}