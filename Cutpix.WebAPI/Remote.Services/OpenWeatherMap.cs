﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using Cutpix.WebAPI.Infrastructure;


namespace Cutpix.WebAPI.Remote.Services
{
    public class OpenWeatherMap : IWeatherApi
    {
        private const string WeatherDataPath = "data/2.5/weather";
        private const string ForecastDataPath = "data/2.5/forecast";

        private readonly UriBuilder _builder;
        private readonly HttpMessenger _messenger;

        public string Host { get; }
        public string ApiKey { get; }

        public OpenWeatherMap(NameValueCollection settings)
        {
            Host = settings.Get("serverName");
            settings.Remove("serverName");

            ApiKey = settings.Get("apiKey");
            settings.Remove("apiKey");

            _builder = new UriBuilder("http", Host);

            _messenger = new HttpMessenger();
        }

        public HttpResponseMessage GetCurrentWeather(string qeury)
        {
            _builder.Path = WeatherDataPath;
            _builder.Query = "q=" + qeury + "&appid=" + ApiKey;

            var request = WebRequest.CreateHttp(_builder.Uri);

            return _messenger.Handle(request);
        }

        public string GetData(string query)
        {
            throw new NotImplementedException();
        }

        public static IWeatherApi CreateInstance(string serverName, string apiKey)
        {
            var settings = new NameValueCollection
            {
                { "serverName", serverName },
                { "apiKey", apiKey }
            };

            return new OpenWeatherMap(settings);
        }
    }
}