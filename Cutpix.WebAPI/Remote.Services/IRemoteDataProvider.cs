﻿namespace Cutpix.WebAPI.Remote.Services
{
    public interface IRemoteDataProvider
    {
        string GetData(string query);
    }
}