﻿using System.Net.Http;

namespace Cutpix.WebAPI.Remote.Services
{
    public interface IWeatherApi : IRemoteDataProvider
    {
        string Host { get; }
        string ApiKey { get; }
        HttpResponseMessage GetCurrentWeather(string qeury);
    }
}