﻿using System.Web.Http;
using System.Web.Http.Filters;

namespace Cutpix.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var provider = new ActionDescriptorFilterProvider();
            config.Services.Add(typeof (IFilterProvider), provider);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{url}",
                defaults: new {url = RouteParameter.Optional}
                );
        }
    }
}