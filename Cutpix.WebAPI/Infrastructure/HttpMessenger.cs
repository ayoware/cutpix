﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Cutpix.WebAPI.Infrastructure
{
    public class HttpMessenger
    {
        public HttpResponseMessage Handle(HttpWebRequest request)
        {
            var response = (HttpWebResponse)request.GetResponse();
            var httpResponse = new HttpResponseMessage(response.StatusCode);

            using (var respStream = response.GetResponseStream())
            {
                if (respStream == null)
                {
                    httpResponse.StatusCode = HttpStatusCode.NotFound;
                    return httpResponse;
                }

                using (var reader = new StreamReader(respStream))
                {
                    var objText = reader.ReadToEnd();
                    httpResponse.Content = new StringContent(objText, Encoding.UTF8, "application/json");

                    response.Close();
                }

                return httpResponse;
            }
        }
    }
}