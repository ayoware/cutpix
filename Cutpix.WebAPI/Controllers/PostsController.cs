﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cutpix.Domain.Models.Blog;
using Cutpix.Services.Interfaces;

namespace Cutpix.WebAPI.Controllers
{
    public class PostsController : ApiController
    {
        private readonly IPostService _postService;

        public PostsController(IPostService postService)
        {
            this._postService = postService;
        }

        // GET api/posts
        public IHttpActionResult Get()
        {
            var lastPosts = _postService.GetLastPosts(DateTime.UtcNow, 10);

            return Json(lastPosts);
        }

        // GET api/posts/url
        public IHttpActionResult Get(string url)
        {
            var post = _postService.FindPostByLink(url);
            if (post == null)
                return NotFound();

            return Json(post);
        }

        public HttpResponseMessage Post([FromBody] Post model)
        {
            _postService.AddNewPost(model);
            var response = Request.CreateResponse(HttpStatusCode.Created, model);

            var uri = Url.Link("DefaultApi", new {url = model.Permalink});
            response.Headers.Location = new Uri(uri);

            return response;
        }
    }
}