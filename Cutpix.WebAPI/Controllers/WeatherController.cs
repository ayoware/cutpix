﻿using System.Collections.Specialized;
using System.Net.Http;
using System.Web.Http;
using Cutpix.WebAPI.Remote.Services;

namespace Cutpix.WebAPI.Controllers
{
    public class WeatherController : ApiController
    {
        private IWeatherApi _weatherApi;

        public WeatherController()
        {
            _weatherApi = OpenWeatherMap.CreateInstance("api.openweathermap.org", "50e287f53f0aeeb2e7b205a50a9ffc20");
        }

        public HttpResponseMessage Get(string q)
        {
            return _weatherApi.GetCurrentWeather(q);
        }
    }
}