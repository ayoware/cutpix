﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Cutpix.Domain.Models
{
    public abstract class EntityBase<TKey> : IEntity<TKey>
    {
        [BsonId, BsonRepresentation(BsonType.ObjectId)]
        public TKey Id { get; set; }
    }
}