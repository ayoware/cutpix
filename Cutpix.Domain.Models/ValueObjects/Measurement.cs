﻿namespace Cutpix.Domain.Models.ValueObjects
{
    public class Measurement
    {
        public double HighValue { get; set; }
        public double LowValue { get; set; }
    }
}