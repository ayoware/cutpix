﻿namespace Cutpix.Domain.Models
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}