﻿namespace Cutpix.Domain.Models.Blog
{
    public class ImageContent : BinaryContent
    {
        private readonly byte[] _imageBytes;

        public ImageContent(byte[] binaryData)
        {
            _imageBytes = binaryData;
        }

        public override byte[] GetBinaryData()
        {
            return _imageBytes;
        }
    }
}