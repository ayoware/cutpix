﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Cutpix.Domain.Models.Blog
{
    public class Post : EntityBase<string>
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string ImageId { get; set; }

        public string Type { get; set; }
        public string Permalink { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public string Author { get; set; }
        public List<Content> ContentList { get; set; }
        public List<string> Tags { get; set; }
        public List<Comment> Comments { get; set; }

        public Post()
        {
            this.Comments = new List<Comment>();
            this.Tags = new List<string>();
            this.ContentList = new List<Content>();
        }
    }
}