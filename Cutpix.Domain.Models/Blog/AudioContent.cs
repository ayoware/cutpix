﻿using MongoDB.Bson.Serialization.Attributes;

namespace Cutpix.Domain.Models.Blog
{
    public class AudioContent : BinaryContent
    {
        [BsonIgnore]
        public byte[] AudioBytes { get; set; }

        public override byte[] GetBinaryData()
        {
            throw new System.NotImplementedException();
        }
    }
}