﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace Cutpix.Domain.Models.Blog
{
    [BsonKnownTypes(typeof (AudioContent), typeof (ImageContent))]
    public abstract class BinaryContent : Content, IEntity<string>
    {
        public virtual string Id { get; set; }
        public abstract byte[] GetBinaryData();
    }
}