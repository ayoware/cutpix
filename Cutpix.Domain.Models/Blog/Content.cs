﻿using MongoDB.Bson.Serialization.Attributes;

namespace Cutpix.Domain.Models.Blog
{
    [BsonDiscriminator(RootClass = true, Required = true)]
    [BsonKnownTypes(typeof (TextContent), typeof (BinaryContent))]
    public abstract class Content
    {
        public virtual string Name { get; set; }
    }
}