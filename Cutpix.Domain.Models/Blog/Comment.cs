﻿using System;

namespace Cutpix.Domain.Models.Blog
{
    public class Comment
    {
        public string Author { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}