﻿using System;
using System.Text;

namespace Cutpix.Domain.Models.Blog
{
    public class TextContent : Content
    {
        public string Body { get; set; }

        public override string ToString()
        {
            var builder = new StringBuilder();

            builder.AppendLine(Name);
            builder.AppendLine(Body);

            return builder.ToString();
        }
    }
}