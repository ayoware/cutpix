﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Cutpix.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        bool Update(T entity, string field, object value);
        bool Replace(T entity);
        bool Delete(object id);
        T Single(object key);
        T Single(Expression<Func<T, bool>> expression);
        IEnumerable<T> All(Expression<Func<T, bool>> filter = null);
    }
}