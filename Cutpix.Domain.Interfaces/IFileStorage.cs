﻿using System.IO;

namespace Cutpix.Domain.Interfaces
{
    public interface IFileStorage
    {
        object AddFile(Stream stream, string fileName);

        Stream Open(object id);
        string GetFileName(object key);
    }
}