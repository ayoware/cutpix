﻿using AutoMapper;
using Cutpix.Domain.Models.Blog;
using Cutpix.Infrastructure.Identity.Owin;
using Cutpix.Web.ViewModels.Blog;
using Cutpix.Web.ViewModels.Dashboard;

namespace Cutpix.Web.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Content, ContentViewModel>();

            CreateMap<TextContent, TextContentViewModel>()
                .IncludeBase<Content, ContentViewModel>();

            CreateMap<ImageContent, ImageContentInfoModel>()
                .IncludeBase<Content, ContentViewModel>();

            CreateMap<Post, PostDetailedViewModel>()
                .ForMember(vm => vm.ContentList, opt => opt.MapFrom(m => m.ContentList));

            CreateMap<Post, PostSummaryViewModel>()
                .ForMember(vm => vm.Category, opt => opt.MapFrom(m => m.Type));

            CreateMap<ApplicationUser, ApplicationUserSummeryModel>();
        }
    }
}