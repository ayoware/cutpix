﻿using System.IO;
using AutoMapper;
using Cutpix.Domain.Models.Blog;
using Cutpix.Web.ViewModels.Blog;


namespace Cutpix.Web.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<ContentViewModel, Content>();

            CreateMap<TextContentViewModel, TextContent>()
                .IncludeBase<ContentViewModel, Content>();

            CreateMap<ImageContentFormModel, ImageContent>()
                .ConstructUsing(vm =>
                {
                    var target = new MemoryStream();
                    vm.File.InputStream.CopyTo(target);

                    return new ImageContent(target.ToArray());
                })
                .IncludeBase<ContentViewModel, Content>();

            CreateMap<NewPostFormViewModel, Post>();
        }
    }
}