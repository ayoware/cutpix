﻿using AutoMapper;

namespace Cutpix.Web.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void ConfigureProfiles()
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<DomainToViewModelMappingProfile>();
                config.AddProfile<ViewModelToDomainMappingProfile>();
            });
        }
    }
}