﻿using AutoMapper;

namespace Cutpix.Web.AutoMapper
{
    public static class MappingConverter
    {
        public static TDestination ConvertTo<TSource, TDestination>(this TSource entity)
        {
            return Mapper.Map<TSource, TDestination>(entity);
        }
    }
}